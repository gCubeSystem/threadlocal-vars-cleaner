
# Changelog for /  threadlocal-vars-cleaner

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.3.1] - 2021-05-25

- Feature #21503 Update threadlocal-vars-cleaner to support AccessTokenProvider

## [v2.3.0] - 2021-05-03

- Fix Bug #20591 Keycloak-LR 6.2 Integration: portlet calls in landing page have not UMA token set

## [v2.2.1] - 2020-12-17

- Added support for OIDC related communications

- Fixed wrong token put in cache after refresh. Rationalized logs.

- Ported to git

## [v2.1.1] - 2020-03-26

- Small fix which checks if the current Liferay Site is a valid context and avoid to call auth if it is not

## [v2.1.0] - 2015-02-22

- Added automatic injection of context and token of any AJAX calls

## [v2.0.0] - 2016-06-23

- First release after switch to new liferay version (6.2.6)
